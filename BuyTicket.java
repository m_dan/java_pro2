import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BuyTicket extends JFrame{
	//user info
	String[] userData = null;
	//create J~
	JLabel listTitle;
	JLabel user;
	JLabel numberLabel;
	JTextArea detailLabel;
	JTextField number;
	JLabel concertListLabel;
	JList concertList;
	JButton detail;
	JButton buyTicket;
	JLabel error;
	JButton myPageButton;

	DefaultListModel concertListModel;
	
	class DetailButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String[] concertData = null;
			String selected = (String) concertList.getSelectedValue();
			String[] concertId = selected.split(":");
			try{
				File concertFile = new File("src/concert.txt");
				BufferedReader br = new BufferedReader(new FileReader(concertFile));
				
				String line;
				br.readLine();
				while((line = br.readLine()) != null){
					concertData = line.split(",");
					if(concertData[0].equals(concertId[0])){
						break;
					}
				}
				
				StringBuffer sb = new StringBuffer();
				sb.append("id:" + concertData[0] + "\n");
				sb.append("name:" + concertData[1] + "\n");
				sb.append("genre:" + concertData[2] + "\n");
				sb.append("artist:" + concertData[3] + "\n");
				sb.append("date:" + concertData[4] + "/" + concertData[5] + "/" + concertData[6] + "\n");
				sb.append("place:" + concertData[7] + "\n");
				sb.append("plice:" + concertData[8] + "\n");
				sb.append("number:" + concertData[10] + "/" + concertData[9] + "\n");
				
				detailLabel.setText(new String(sb));
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}

		}
	}

	class BuyTicketButtonAction implements ActionListener{
		int userId;
		BuyTicketButtonAction(int userId){
			super();
			this.userId = userId;
		}
		public void actionPerformed(ActionEvent e){
			String selected = (String) concertList.getSelectedValue();
			
			if(selected == null){
				error.setText("Please select a concert");
				return;
			}
			try{
				Integer.parseInt(number.getText());
			}catch(NumberFormatException ne){
				error.setText("Please set the number of tickets");
				return;
			}
			String[] concertId = selected.split(":");
			StringBuffer writeSb = new StringBuffer();
			try{
				
				//add number of person
				File file = new File("src/concert.txt");
				BufferedReader br = new BufferedReader(new FileReader(file));

				String line = br.readLine();
				writeSb.append(line + "\n");
				while((line = br.readLine()) != null){
					String data[] = line.split(",");
					
					if(data[0].equals(concertId[0])){
						if((Integer.parseInt(data[10]) + Integer.parseInt(number.getText())) - Integer.parseInt(data[9]) > 0){
							error.setText("the number is too large");
							br.close();
							return;
						}
						data[10] = String.valueOf(Integer.parseInt(data[10]) + Integer.parseInt(number.getText()));
						
						String separator = ",";
						line = String.join(separator, data);

					}
					writeSb.append(line + "\n");
				}
				FileWriter fw = new FileWriter(file);
				fw.write(writeSb.toString());
				br.close();
				fw.close();

				
				file = new File("src/reservation.txt");
				br = new BufferedReader(new FileReader(file));
				writeSb = new StringBuffer();
				
				Boolean newFlag = true;
				String lastReserveId = null;
				while((line = br.readLine()) != null){
					String data[] = line.split(",");
					
					if(data[1].equals(String.valueOf(userId)) && data[2].equals(concertId[0]) && data[4].equals("0")){
						data[3] = String.valueOf(Integer.parseInt(data[3]) + Integer.parseInt(number.getText()));
						newFlag = false;
						
						String separator = ",";
						line = String.join(separator, data);

					}
					
					
					lastReserveId = data[0];

					writeSb.append(line + "\n");
				}
				
				
				if(newFlag){
					String reserve = String.valueOf(Integer.parseInt(lastReserveId) + 1) + ","
						+ String.valueOf(userId) + "," + concertId[0] + "," + number.getText() + ",0";
					writeSb.append(reserve);
				}
				
				fw = new FileWriter(file);
				fw.write(writeSb.toString());
				
				br.close();
				fw.close();
				
								
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}
			
			JFrame frame = new JFrame();
			JLabel label = new JLabel("Buy Ticket!");
			label.setForeground(Color.RED);
			JOptionPane.showMessageDialog(frame, label, "Dialog", JOptionPane.PLAIN_MESSAGE);
			
			error.setText(" ");
			return;

		}
	}

	class MyPageButtonAction implements ActionListener{
		int userId;
		MyPageButtonAction(int userId){
			super();
			this.userId = userId;
		}
		public void actionPerformed(ActionEvent e){
			setVisible(false);
			UserControl userControl = new UserControl("MyPage", userId);
			userControl.setVisible(true);
		}
	}

	
	BuyTicket(String title, int userId){
		//set window
		setSize(800, 600);
		setLocationRelativeTo(null);
		setTitle(title);

		//get user info
		try{
			File userFile = new File("src/user.txt");
			BufferedReader br = new BufferedReader(new FileReader(userFile));
			
			String userLine;
			while((userLine = br.readLine()) != null){
				userData = userLine.split(",");
				if(Integer.parseInt(userData[0]) == userId){
					break;
				}
			}
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);;
		}
		
		//new J~
		listTitle = new JLabel("your ticket");
		user = new JLabel("your name:" + userData[1]);
		detail = new JButton("detail");
		number = new JTextField(1);
		numberLabel = new JLabel("number of ticket");
		number.setPreferredSize(new Dimension(1, 1));
		buyTicket = new JButton("Buy Ticket!");
		detailLabel = new JTextArea();
		detailLabel.setEditable(false);
		concertListModel = new DefaultListModel();
	    getConcert();//get list
		concertList = new JList(concertListModel);
		JScrollPane sp = new JScrollPane();
		error = new JLabel();
		error.setForeground(Color.RED);
		myPageButton = new JButton("MyPage");
		
		//ScrollPane setting
	    sp.getViewport().setView(concertList);
	    sp.setPreferredSize(new Dimension(200, 150));
	    
	    //set button action
	    DetailButtonAction DetailButtonListener = new DetailButtonAction();
		detail.addActionListener(DetailButtonListener);
		BuyTicketButtonAction BuyTicketButtonListener = new BuyTicketButtonAction(userId);
		buyTicket.addActionListener(BuyTicketButtonListener);
		MyPageButtonAction MyPageButtonListener = new MyPageButtonAction(userId);
		myPageButton.addActionListener(MyPageButtonListener);
		

	    
		
		
		//create panel
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder( 30, 60, 20, 60 ));
		panel.setLayout(new GridLayout(1,0));
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		JPanel numberPanel = new JPanel();
		numberPanel.setLayout(new BoxLayout(numberPanel, BoxLayout.LINE_AXIS));
		
		//set panel
		numberPanel.add(numberLabel);
		numberPanel.add(number);
		leftPanel.add(user);
		leftPanel.add(listTitle);
		leftPanel.add(sp);
		leftPanel.add(detail);
		rightPanel.add(detailLabel);
		rightPanel.add(numberPanel);
		rightPanel.add(buyTicket);
		rightPanel.add(error);
		rightPanel.add(myPageButton);
		panel.add(leftPanel);
		panel.add(rightPanel);

		//create container
		Container pane = getContentPane();
		pane.add(panel);
		



	}
	
	private void getConcert(){
		ArrayList<String> concertId = new ArrayList<String>();
		Map numberOfTicket = new HashMap<String, String>();
		try{
			//get concert data
			File file = new File("src/concert.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			br.readLine();
			String line;
			while((line = br.readLine()) != null){
				String[] concertData = line.split(",");
				//set concert in ListModel
				concertListModel.addElement(concertData[0] + ":" + concertData[1]);
			}


		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);;
		}
		return;

	}

}
