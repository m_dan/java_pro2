import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ConcertSystem extends JFrame{

	
	class ButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Login login = new Login("Login");
			login.setVisible(true);
		}
		
		
	}
	
	class AdminAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			ReservationSystem  admin = new ReservationSystem ("ReservationSystem");
			admin.setVisible(true);
		}
		
	}

	ConcertSystem(String title){
		setTitle(title);
		setSize(300, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder( 60, 60, 20, 60 ));
		panel.setLayout(new GridLayout(0,1));

		JButton btn = new JButton("admin");
		JButton btn2 = new JButton("member");
		
		AdminAction adminListener = new AdminAction();
		ButtonAction buttonListener = new ButtonAction();
		btn2.addActionListener(buttonListener);
		btn.addActionListener(adminListener);
		
		panel.add(btn);
		panel.add(btn2);
		Container pane = getContentPane();
		pane.add(panel);
	}
}
