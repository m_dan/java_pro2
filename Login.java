import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Login extends JFrame{
	//create J~
	JPanel panel;
	JLabel labelName;
	JLabel labelPass;
	JTextField text;
	JPasswordField pass;
	JLabel error;
	JLabel user;
	JLabel password;
	JButton newAccount;
	JButton btn;
	
	public static void main(String[] args){
		Login login = new Login("Login");
		login.setVisible(true);
	}
	
	class NewAccountButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setVisible(false);
			SignUp signUp = new SignUp("Sign Up");
			signUp.setVisible(true);
		}
	}

	class LoginButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//get input
			String name = text.getText();
			char[] inputPass = pass.getPassword();
			String inputPassStr = new String(inputPass);
			
			if(name.equals("root") && inputPassStr.equals("root")){
				ReservationSystem  admin = new ReservationSystem ("ReservationSystem");
				admin.setVisible(true);
				return;
			}
			
			//search user
			try{
				File userFile = new File("src/user.txt");
				BufferedReader br = new BufferedReader(new FileReader(userFile));
				
				String userLine;
				String[] userData = null;
				while((userLine = br.readLine()) != null){
					userData = userLine.split(",");
					if(userData[1].equals(name)){
						if(userData[2].equals(inputPassStr)){
							//open UserControl
							UserControl userControl = new UserControl("MyPage", Integer.parseInt(userData[0]));
							userControl.setVisible(true);
							break;
						}else{
							error.setText("password is incorrect");
							error.setForeground(Color.RED);
							break;
						}
					}else{
						error.setText("user not found");
						error.setForeground(Color.RED);
					}
				}
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}

			
			
			
			
		}
	}

	Login(String title){
		//set window
		setTitle(title);
		setSize(300, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//new J~
		labelName = new JLabel("user");
		labelPass = new JLabel("password");
		error = new JLabel();
		text = new JTextField();
		pass = new JPasswordField();
		newAccount = new JButton("new account");
		
		//panel layout
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder( 30, 60, 20, 60 ));
		panel.setLayout(new GridLayout(0,1));
		btn = new JButton("login");
		
		//set button action
		LoginButtonAction loginButtonListener = new LoginButtonAction();
		btn.addActionListener(loginButtonListener);
		NewAccountButtonAction newAccountButtonListener = new NewAccountButtonAction();
		newAccount.addActionListener(newAccountButtonListener);

		//set panel
		panel.add(labelName);
		panel.add(text);
		panel.add(labelPass);
		panel.add(pass);
		panel.add(btn);
		panel.add(error);
		panel.add(newAccount);
		
		//create container
		Container pane = getContentPane();
		pane.add(panel);
	
	}
	
}
