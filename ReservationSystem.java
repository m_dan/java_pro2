/* Version 6-25 17:52 */
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;


public class ReservationSystem extends JFrame{
	
	//static final int factorNum = 12;
	//static final int appearNum = 10; // appear

	private DefaultListModel<String> resListModel;
	private JList<String> resList; 
	
	private JTextField txtID;
	private JTextField txtName;
	private JTextField txtGenre;
	private JTextField txtArtist;
	private JTextField txtYear;
	private JTextField txtMonth;
	private JTextField txtDay;
	private JTextField txtPlace;
	private JTextField txtPrice;
	private JTextField txtMaxPerson;
	
	
	ReservationSystem(String title){
	setTitle(title);
	setSize(800, 800);
	setLocationRelativeTo(null);

	// make list model
	resListModel = new DefaultListModel<String>();
	
	// make JList with scroll panel
	resList = new JList<String>(resListModel);
	resList.setVisibleRowCount(20);
	resList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	JScrollPane resListPane = new JScrollPane(resList); 
	resListPane.createVerticalScrollBar();
	resListPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	 
	 // make JPanel 
	JPanel resPane = new JPanel();
	resPane.setBorder(BorderFactory.createEmptyBorder( 60, 60, 20, 60 ));
	resPane.setLayout(new GridLayout(1,0));
	
	//JPanel formPane[] = new JPanel[appearNum];
	JPanel subPane = new JPanel();
	
	JPanel paneID = new JPanel();
	JPanel paneName = new JPanel();
	JPanel paneGenre = new JPanel();
	JPanel paneArtist = new JPanel();
	JPanel paneYear = new JPanel();
	JPanel paneMonth = new JPanel();
	JPanel paneDay = new JPanel();
	JPanel panePlace = new JPanel();
	JPanel panePrice = new JPanel();
	JPanel paneMaxPerson = new JPanel();
	JPanel paneBtn = new JPanel();
	JPanel paneTopBtn = new JPanel();
	
	// make input field

	txtID = new JTextField("");
	txtID.setEditable(false);
	txtName = new JTextField("");
	txtGenre = new JTextField("");
	txtArtist = new JTextField("");
	txtYear = new JTextField("");	
	txtMonth = new JTextField("");
	txtDay = new JTextField("");
	txtPlace = new JTextField("");
	txtPrice = new JTextField("");
	txtMaxPerson = new JTextField(""); 
	
	// make label
	JLabel labID = new JLabel("id");
	JLabel labName = new JLabel("name");
	JLabel labGenre = new JLabel("genre");
	JLabel labArtist = new JLabel("artist");
	JLabel labYear = new JLabel("year");	
	JLabel labMonth = new JLabel("month");
	JLabel labDay = new JLabel("day");
	JLabel labPlace = new JLabel("place");
	JLabel labPrice = new JLabel("price");
	JLabel labMaxPerson = new JLabel("max person"); 
	
	// make button
	JButton dtBtn = new JButton("Detail");
	JButton rmBtn = new JButton("Remove");
	JButton cncBtn = new JButton("Create New Concert");
	
	
	// Setup list
	try{
		File userFile = new File("src/concert.txt");
		BufferedReader brRS = new BufferedReader(new FileReader(userFile));
		
		String userLine;
		String[] resData = null;
		brRS.readLine();
		while((userLine = brRS.readLine()) != null) {
			resData = userLine.split(",");
			if (resData[11].equals("0"))
			resListModel.addElement(resData[0] + ": " + resData[4] +"-"+ resData[5] +"-" + resData[6] + "  " +resData[1]);
		}
		brRS.close();
	}catch(FileNotFoundException e1){
		System.out.println(e1);
	}catch(IOException e1){
		System.out.println(e1);
	}

	
	// Check Details
	class RmAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int index = resList.getSelectedIndex();
				if (index != -1) { 
					resListModel.remove(index);
					try{
						File userFile = new File("src/concert.txt");
						BufferedReader brRS = new BufferedReader(new FileReader(userFile));
						String userLine;
						StringBuffer bufferLine = new StringBuffer();
						
						int i;
						for (i = -1 ;(userLine = brRS.readLine()) != null; i++) {
							// add remove flag
							if (i == index) {
								String[] resData1 = null;
								resData1 = userLine.split(",");
								resData1[11] = "1";
								int j;
								for (j = 0; j < resData1.length; j++) {
									if (j != 0)
										bufferLine.append(",");
									bufferLine.append(resData1[j]);
								}
									bufferLine.append(System.getProperty("line.separator"));
								// append on bufferLine
							} else {
								bufferLine.append(userLine);
								bufferLine.append(System.getProperty("line.separator"));
							}
						}
						brRS.close();
						// Write to file
						BufferedWriter bwRS = new BufferedWriter(new FileWriter(userFile));
						bwRS.write(bufferLine.toString());
						bwRS.close();
					}catch(FileNotFoundException e1){
						System.out.println(e1);
					}catch(IOException e1){
						System.out.println(e1);
					}
					
			} else 
				JOptionPane.showMessageDialog(resList, "None selected!", "Error",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	
	
	
	
	class CncAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if (dtBtn.getText().equals("Detail")) { 	
					 try{
						 // ready to add
						File userFile = new File("src/concert.txt");
						BufferedReader rLine = new BufferedReader(new FileReader(userFile));
						int newID;
						newID = Integer.parseInt(rLine.readLine()) + 1;
						StringBuffer cncString = new StringBuffer();
						String cncLine; 
						// replace list data
						cncString.append(newID);
						cncString.append(System.getProperty("line.separator"));
						while ((cncLine = rLine.readLine()) != null) {
							cncString.append(cncLine);
							cncString.append(System.getProperty("line.separator"));
						}
						rLine.close();
						cncString.append(newID);
						cncString.append(", , , , , , , , , ,0,0");
						// set text
						txtID.setText(String.valueOf(newID));
						txtName.setText("");
						txtGenre.setText("");
						txtArtist.setText("");
						txtYear.setText("");
						txtMonth.setText("");
						txtDay.setText("");
						txtPlace.setText("");
						txtPrice.setText("");
						txtMaxPerson.setText("");
						dtBtn.setText("Save");
						// Write to file
						BufferedWriter cncBw = new BufferedWriter(new FileWriter(userFile));
						cncBw.write(cncString.toString());
						cncBw.close();
					} catch(FileNotFoundException e1) {
						System.out.println(e1);
					} catch(IOException e1) {
						System.out.println(e1);
					}
				}  else {
				JOptionPane.showMessageDialog(resList, "Please save before create new Concert!!", "Error",JOptionPane.ERROR_MESSAGE);
			}
		}		
	}
	
	

	
	
	
	class DtAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if (dtBtn.getText().equals("Detail")) { 	
				int index = resList.getSelectedIndex();
				if (index != -1) { 
					 try{
						 // detect edit ID
						String editLine = resList.getSelectedValue();
						String[] editData = null;
						int editLineNum;
						editData = editLine.split(":");
						editLineNum = Integer.parseInt(editData[0]);
						
						 // search ID
						File userFile = new File("src/concert.txt");
						BufferedReader rLine = new BufferedReader(new FileReader(userFile));
						rLine.readLine();
						
						String detailLine; 
						String[] detailData = null;	
						
						// search ID
						while ((detailLine = rLine.readLine()) != null)  {  
							detailData = detailLine.split(",");
							if (editLineNum == Integer.parseInt(detailData[0])) 
								break;
						}
						rLine.close();
						// set text
						txtID.setText(detailData[0]);
						txtName.setText(detailData[1]);
						txtGenre.setText(detailData[2]);
						txtArtist.setText(detailData[3]);
						txtYear.setText(detailData[4]);
						txtMonth.setText(detailData[5]);
						txtDay.setText(detailData[6]);
						txtPlace.setText(detailData[7]);
						txtPrice.setText(detailData[8]);
						txtMaxPerson.setText(detailData[9]);
						dtBtn.setText("Save");
					} catch(FileNotFoundException e1) {
						System.out.println(e1);
					} catch(IOException e1) {
						System.out.println(e1);
					}
				} else 
					JOptionPane.showMessageDialog(resList, "Please select the row!", "Error",JOptionPane.ERROR_MESSAGE);
		
			} else {	
				
				try{
					File userFile = new File("src/concert.txt");
					BufferedReader brRS = new BufferedReader(new FileReader(userFile));
					String userLine;
					StringBuffer svString = new StringBuffer();
					svString.append(brRS.readLine());
					svString.append(System.getProperty("line.separator"));
					while ((userLine = brRS.readLine()) != null) {
						String svData[] = null;
						svData = userLine.split(",");	
						if (svData[0].equals(txtID.getText())) {
						// add change
							svString.append(txtID.getText());
							svString.append(",");
							svString.append(txtName.getText());
							svString.append(",");
							svString.append(txtGenre.getText());
							svString.append(",");
							svString.append(txtArtist.getText());
							svString.append(",");
							svString.append(txtYear.getText());
							svString.append(",");
							svString.append(txtMonth.getText());
							svString.append(",");
							svString.append(txtDay.getText());
							svString.append(",");
							svString.append(txtPlace.getText());
							svString.append(",");
							svString.append(txtPrice.getText());
							svString.append(",");
							svString.append(txtMaxPerson.getText());
							svString.append(",");
							svString.append(svData[10]);
							svString.append(",");
							svString.append(svData[11]);
							svString.append(System.getProperty("line.separator"));
							// append on bufferLine
						} else {
							svString.append(userLine);
							svString.append(System.getProperty("line.separator"));
						}
					}
					brRS.close();
					// Write to file
					BufferedWriter bwRS = new BufferedWriter(new FileWriter(userFile));
					bwRS.write(svString.toString());
					bwRS.close();
					dtBtn.setText("Detail");
				}catch(FileNotFoundException e1){
					System.out.println(e1);
				}catch(IOException e1){
					System.out.println(e1);
				}
				
				resListModel.clear();
				// Setup list
				try{
					File userFile = new File("src/concert.txt");
					BufferedReader brRS = new BufferedReader(new FileReader(userFile));
					
					String userLine;
					String[] resData = null;
					brRS.readLine();
					while((userLine = brRS.readLine()) != null) {
						resData = userLine.split(",");
						if (resData[11].equals("0"))
						resListModel.addElement(resData[0] + ": " + resData[4] +"-"+ resData[5] +"-" + resData[6] + "  " +resData[1]);
					}
					brRS.close();
				}catch(FileNotFoundException e1){
					System.out.println(e1);
				}catch(IOException e1){
					System.out.println(e1);
				}

			}
		}		
	}
	
	
	//set action listener
	RmAction rmButtonListener = new RmAction(); 
	rmBtn.addActionListener(rmButtonListener);
	
	DtAction dtButtonListener = new DtAction(); 
	dtBtn.addActionListener(dtButtonListener);
	
	CncAction cncButtonListener = new CncAction(); 
	cncBtn.addActionListener(cncButtonListener);
	
	
	// make sub panel factors
	
	paneID.setLayout(new GridLayout(1,0));
	paneID.add(labID);
	paneID.add(txtID);
	paneName.setLayout(new GridLayout(1,0));
	paneName.add(labName);
	paneName.add(txtName);
	paneGenre.setLayout(new GridLayout(1,0));
	paneGenre.add(labGenre);
	paneGenre.add(txtGenre);
	paneArtist.setLayout(new GridLayout(1,0));
	paneArtist.add(labArtist);
	paneArtist.add(txtArtist);
	paneYear.setLayout(new GridLayout(1,0));
	paneYear.add(labYear);
	paneYear.add(txtYear);
	paneMonth.setLayout(new GridLayout(1,0));
	paneMonth.add(labMonth);
	paneMonth.add(txtMonth);
	paneDay.setLayout(new GridLayout(1,0));
	paneDay.add(labDay);
	paneDay.add(txtDay);
	panePlace.setLayout(new GridLayout(1,0));
	panePlace.add(labPlace);
	panePlace.add(txtPlace);
	panePrice.setLayout(new GridLayout(1,0));
	panePrice.add(labPrice);
	panePrice.add(txtPrice);
	paneMaxPerson.setLayout(new GridLayout(1,0));
	paneMaxPerson.add(labMaxPerson);
	paneMaxPerson.add(txtMaxPerson);
	
	paneTopBtn.add(cncBtn,BorderLayout.CENTER);
	paneBtn.add(rmBtn, BorderLayout.WEST);
	paneBtn.add(dtBtn, BorderLayout.EAST);
	// make sub panel
	subPane.setLayout(new GridLayout(0,1));	
	subPane.add(paneTopBtn);
	
	subPane.add(paneID);
	subPane.add(paneName);
	subPane.add(paneGenre);
	subPane.add(paneArtist);
	subPane.add(paneYear);	
	subPane.add(paneMonth);
	subPane.add(paneDay);
	subPane.add(panePlace);
	subPane.add(panePrice);
	subPane.add(paneMaxPerson); 
	
	subPane.add(paneBtn);
	// fusion panel
	resPane.add(subPane);
	resPane.add(resListPane);
	Container panel = getContentPane();
	panel.add(resPane);
	
	}

}



