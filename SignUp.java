import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SignUp extends JFrame{
	Container pane;
	//create J~
	JPanel panel;
	JLabel labelName;
	JLabel labelPass;
	JTextField text;
	JPasswordField pass;
	JLabel error;
	JButton btn;
	JButton signUp;
	
	class SignUpAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//get input
			String name = text.getText();
			char[] inputPass = pass.getPassword();
			String inputPassStr = new String(inputPass);
			
			//search user
			try{
				File userFile = new File("src/user.txt");
				BufferedReader br = new BufferedReader(new FileReader(userFile));
				
				String userLine;
				String[] userData = null;
				StringBuffer writeSb = new StringBuffer();
				String lastId = null;
				while((userLine = br.readLine()) != null){
					userData = userLine.split(",");
					if(userData[1].equals(name)){
						error.setText("this name is used");
						error.setForeground(Color.RED);
						return;
					}else{
						writeSb.append(userLine + "\n");
						String[] data = userLine.split(",");
						lastId = data[0];
					}
				}
				
				String newUser = String.valueOf(Integer.parseInt(lastId) + 1) + "," + name + "," + inputPassStr;
				writeSb.append(newUser);
				
				FileWriter fw = new FileWriter(userFile);
				fw.write(writeSb.toString());
				br.close();
				fw.close();
				
				JFrame frame = new JFrame();
				JLabel label = new JLabel("Sign Up!");
				label.setForeground(Color.RED);
				JOptionPane.showMessageDialog(frame, label);

				setVisible(false);
				UserControl userControl = new UserControl("MyPage", Integer.parseInt(lastId) + 1);
				userControl.setVisible(true);

				
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}

			
			
			
			
		}
	}

	SignUp(String title){
		//set window
		setTitle(title);
		setSize(300, 300);
		setLocationRelativeTo(null);
		
		//new J~
		labelName = new JLabel("user");
		labelPass = new JLabel("password");
		error = new JLabel();
		text = new JTextField();
		pass = new JPasswordField();
		
		//panel layout
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder( 30, 60, 20, 60 ));
		panel.setLayout(new GridLayout(0,1));
		btn = new JButton("sign up");
		
		//set button action
		SignUpAction signUpListener = new SignUpAction();
		btn.addActionListener(signUpListener);

		//set panel
		panel.add(labelName);
		panel.add(text);
		panel.add(labelPass);
		panel.add(pass);
		panel.add(btn);
		panel.add(error);
		
		//create container
		pane = getContentPane();
		pane.add(panel);
	
	}
	
}
