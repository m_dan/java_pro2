import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UserControl extends JFrame{
	//user info
	String[] userData = null;
	//create J~
	JLabel listTitle;
	JLabel user;
	JTextArea detailLabel;
	JLabel concertListLabel;
	JList concertList;
	JButton detail;
	JButton buyTicket;
	JButton cancel;

	DefaultListModel concertListModel;
	

	class BuyTicketButtonAction implements ActionListener{
		int userId;
		
		BuyTicketButtonAction(int userId){
			super();
			this.userId = userId;
		}
		public void actionPerformed(ActionEvent e){
			BuyTicket buyTicket = new BuyTicket("Ticket Store", userId);
			buyTicket.setVisible(true);
			setVisible(false);
		}
	}
	
	class DetailButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String[] concertData = null;
			String selected = (String) concertList.getSelectedValue();
			if(selected == null){
				return;
			}
			String[] concertId = selected.split(":");
			try{
				File concertFile = new File("src/concert.txt");
				BufferedReader br = new BufferedReader(new FileReader(concertFile));
				
				String line;
				br.readLine();
				while((line = br.readLine()) != null){
					concertData = line.split(",");
					if(concertData[0].equals(concertId[0])){
						break;
					}
				}
				
				StringBuffer sb = new StringBuffer();
				sb.append("id:" + concertData[0] + "\n");
				sb.append("name:" + concertData[1] + "\n");
				sb.append("genre:" + concertData[2] + "\n");
				sb.append("artist:" + concertData[3] + "\n");
				sb.append("date:" + concertData[4] + "/" + concertData[5] + "/" + concertData[6] + "\n");
				sb.append("place:" + concertData[7] + "\n");
				sb.append("plice:" + concertData[8] + "\n");
				sb.append("number:" + concertData[10] + "/" + concertData[9] + "\n");
				
				detailLabel.setText(new String(sb));
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}

		}
	}

	class CancelButtonAction implements ActionListener{
		int userId;
		CancelButtonAction(int userId){
			super();
			this.userId = userId;
		}
		public void actionPerformed(ActionEvent e){
			String[] reservationData = null;
			String selected = (String) concertList.getSelectedValue();
			if(selected == null){
				return;
			}

			String[] concertId = selected.split(":");
			StringBuffer writeSb = new StringBuffer();
			String numberOfTicket = null;
			
			try{
				File file = new File("src/reservation.txt");
				BufferedReader br = new BufferedReader(new FileReader(file));
				
				String line;
				while((line = br.readLine()) != null){
					reservationData = line.split(",");
					if(reservationData[2].equals(concertId[0]) && Integer.parseInt(reservationData[1]) == userId){
						String separator = ",";
						reservationData[4] = "1";
						numberOfTicket = reservationData[3];
						line = String.join(separator, reservationData);
					}
					writeSb.append(line + "\n");
				}
				
				FileWriter fw = new FileWriter(file);
				fw.write(writeSb.toString());
				br.close();
				fw.close();
				
				
				writeSb = new StringBuffer();
				file = new File("src/concert.txt");
				br = new BufferedReader(new FileReader(file));
				line = br.readLine();
				writeSb.append(line + "\n");
				while((line = br.readLine()) != null){
					String data[] = line.split(",");
					
					if(data[0].equals(concertId[0])){
						data[10] = String.valueOf(Integer.parseInt(data[10]) - Integer.parseInt(numberOfTicket));
						
						String separator = ",";
						line = String.join(separator, data);

					}
					writeSb.append(line + "\n");
				}
				fw = new FileWriter(file);
				fw.write(writeSb.toString());
				br.close();
				fw.close();

				
				
				concertListModel.clear();
				getUserConcert(userId);
				
				JFrame frame = new JFrame();
				JLabel label = new JLabel("cancel is completed!");
				label.setForeground(Color.RED);
				JOptionPane.showMessageDialog(frame, label, "Dialog", JOptionPane.PLAIN_MESSAGE);
			

				
			}catch(FileNotFoundException e1){
				System.out.println(e1);
			}catch(IOException e1){
				System.out.println(e1);;
			}

		}
	}


	UserControl(String title, int userId){
		//set window
		setTitle(title);
		setSize(800, 600);
		setLocationRelativeTo(null);
		
		
		//get user info
		try{
			File userFile = new File("src/user.txt");
			BufferedReader br = new BufferedReader(new FileReader(userFile));
			
			String userLine;
			while((userLine = br.readLine()) != null){
				userData = userLine.split(",");
				if(Integer.parseInt(userData[0]) == userId){
					break;
				}
			}
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);;
		}
		
		
		//new J~
		listTitle = new JLabel("your ticket");
		user = new JLabel("your name:" + userData[1]);
		detail = new JButton("detail");
		detailLabel = new JTextArea();
		detailLabel.setEditable(false);
		buyTicket = new JButton("Ticket Store");
		cancel = new JButton("Ticket cancel");
		concertListModel = new DefaultListModel();
	    getUserConcert(userId);//get list
		concertList = new JList(concertListModel);
		JScrollPane sp = new JScrollPane();
		
		//ScrollPane setting
	    sp.getViewport().setView(concertList);
	    sp.setPreferredSize(new Dimension(200, 80));
	    
		
		
		//create panel
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder( 30, 60, 20, 60 ));
		panel.setLayout(new GridLayout(1,0));
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		
		//set button action
		BuyTicketButtonAction BuyTicketButtonListener = new BuyTicketButtonAction(userId);
		buyTicket.addActionListener(BuyTicketButtonListener);
		DetailButtonAction DetailButtonListener = new DetailButtonAction();
		detail.addActionListener(DetailButtonListener);
		CancelButtonAction CancelButtonListener = new CancelButtonAction(userId);
		cancel.addActionListener(CancelButtonListener);

		
		//set button action

		//set panel
		leftPanel.add(user);
		leftPanel.add(listTitle);
		leftPanel.add(sp);
		leftPanel.add(detail);
		rightPanel.add(detailLabel);
		rightPanel.add(cancel);
		rightPanel.add(buyTicket);
		panel.add(leftPanel);
		panel.add(rightPanel);

		//create container
		Container pane = getContentPane();
		pane.add(panel);
		
	}
	
	private void getUserConcert(int userId){
		ArrayList<String> concertId = new ArrayList<String>();
		Map numberOfTicket = new HashMap<String, String>();
		try{
			//get concertId
			File file = new File("src/reservation.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			String line;
			while((line = br.readLine()) != null){
				String[] reservationData = line.split(",");
				if(Integer.parseInt(reservationData[1]) == userId){
					if(Integer.parseInt(reservationData[4]) == 0){
						concertId.add(reservationData[2]);
						numberOfTicket.put(reservationData[2], reservationData[3]);
					}
				}
			}
			
			//get concert data
			file = new File("src/concert.txt");
			br = new BufferedReader(new FileReader(file));
			
			br.readLine();
			while((line = br.readLine()) != null){
				String[] concertData = line.split(",");
				if(concertId.indexOf(concertData[0]) >= 0){
					//set concert in ListModel
					concertListModel.addElement(concertData[0] + ":" + concertData[1] + ":" + numberOfTicket.get(concertData[0]) + "ticket");
				}
			}


		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);;
		}
		return;

	}
	
}
